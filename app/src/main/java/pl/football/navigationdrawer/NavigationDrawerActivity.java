package pl.football.navigationdrawer;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by dkrezel on 2015-05-03.
 */
public class NavigationDrawerActivity extends ActionBarActivity {

	private String[] mColors;
	private DrawerLayout mNavigationDrawer;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private String mPositionTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.navigation_layout);

		mColors = getResources().getStringArray(R.array.colors);
		mNavigationDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer_id);
		mDrawerList = (ListView) findViewById(R.id.left_drawer_id);

		mDrawerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mColors));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);	// dodaje ikonę Home w widoku ActionBar (domyślnie strzałka)
		getSupportActionBar().setHomeButtonEnabled(true);

		setUpDrawer();
	}

	// Implementacja interfejsu odpowiedzialnego za kliknięcie na pozycje w ListView
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			selectItem(position);	//ustawiamy odpowiedni nagłówek w ActionBar
			mNavigationDrawer.closeDrawer(mDrawerList);	// zamykamy Navigation Drawer po kliknięciu na pozycję w liście (ListView)
		}
	}

	private void selectItem(int mPosition) {
		mPositionTitle = mColors[mPosition];
	}

	private void setUpDrawer() {
		mDrawerToggle = new ActionBarDrawerToggle(this, mNavigationDrawer, R.string.drawer_open, R.string.drawer_close) {
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getSupportActionBar().setTitle("Navigation !");
				invalidateOptionsMenu();	// Daje znać o zmianie opcji w menu ActionBar (wywoływane na wypadek zmiany opcji menu)
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				getSupportActionBar().setTitle(mPositionTitle);
				invalidateOptionsMenu();
			}
		};

		mDrawerToggle.setDrawerIndicatorEnabled(true);
		mNavigationDrawer.setDrawerListener(mDrawerToggle);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();	// ustawia odpowiednią ikonę w ActionBar podczas nawigacji Drawer'em
	}
}
